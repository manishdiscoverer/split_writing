
\section{Extension to \splitenc}
\label{sec:extension}

\subsection{Support for MultiD Range Predicates}
\label{subsec:split_do_MD}
%Till now we have seen, how our scheme takes the plaintext table and generates encrypted database and how the 1D range predicate queries are handled. 
We will now extend our scheme to support 2D range predicate queries. The extension for higher number of dimensions is similar. Coming back to our running example in Figure~\ref{fig:custschema}, suppose the \emph{Client} wants to ask a query having simultaneous range predicates over both \emph{Age} and \emph{Balance} columns.

\subsubsection*{Data Organization}
In this case, the \emph{Data Owner} needs to create four tables for each plaintext table, one corresponding to each BS and RS combination of the two columns on which range predicates can occur simultaneously. The encrypted database schema for the example is shown in Figure~\ref{fig:custEnc2D}. Note that the keys used for the BS and RS columns in different tables are different. For example, $Age\_BS$ in $Customer\_BS\_BS$ is encrypted with different keys than $Age\_BS$ in $Customer\_BS\_BS$. Thus there is no correlation between the $Age\_BS$ columns in different tables.

\begin{figure}[h!] 
\begin{framed}
\small
\hspace*{-7pt}\emph{{\bf Customer\_BS\_BS}(Customer\_ID\_E$_{K_1}$, Age\_BS)}\\
\hspace*{-7pt}\emph{{\bf Customer\_BS\_RS}(Customer\_ID\_E$_{K_2}$, Age\_BS)}\\
\hspace*{-7pt}\emph{{\bf Customer\_RS\_BS}(Customer\_ID\_E$_{K_3}$, Age\_RS)}\\
\hspace*{-7pt}\emph{{\bf Customer\_RS\_RS}(Customer\_ID\_E$_{K_4}$, Age\_RS)}\\
\hspace*{-7pt}\emph{{\bf Account\_BS\_BS}(Customer\_ID\_E$_{K_1}$,Branch\_ID\_E$_{K_5}$,Bal\_BS)}\\
\hspace*{-7pt}\emph{{\bf Account\_BS\_RS}(Customer\_ID\_E$_{K_2}$,Branch\_ID\_E$_{K_6}$,Bal\_RS)}\\
\hspace*{-7pt}\emph{{\bf Account\_RS\_BS}(Customer\_ID\_E$_{K_3}$,Branch\_ID\_E$_{K_7}$,Bal\_BS)}\\
\hspace*{-7pt}\emph{{\bf Account\_RS\_RS}(Customer\_ID\_E$_{K_4}$,Branch\_ID\_E$_{K_8}$,Bal\_RS)}
\end{framed}
\caption{\splitenc Encrypted Scheme for 2D range predicates}
\label{fig:custEnc2D}
\end{figure}

In general, if there are $T$ tables and total $n$ columns in the database, and the \emph{Data Owner} wants to support simultaneous range predicates over all of the $n$ columns, then every database table will be encrypted  $2^n$ times, corresponding to all the combinations of BS and RS split of each column leading to a total of $T * 2^n$ tables in the encrypted database. In Section~\ref{subsec:opt} we will show how we can reduce this exponential space blowup.

\subsubsection*{Evaluating Range Queries}
To evaluate a query with predicates on both dimensions, they are split into multiple queries which are evaluated on different BS and RS tables. 
Let $r_1$ be the range on the first dimension ($Age$) and $r_2$ the range on the second dimension ($Balance$). As before, each range is split into a set of prefix based ranges which are mapped to either RS or BS depending on the size of the prefix. Let $RS_1$ and $BS_1$ be the sets of ranges corresponding to $r_1$ and  $RS_2$ and $BS_2$ be sets of ranges corresponding to $r_2$. The equivalent set of ranges is thus $(RS_1 \times RS_2) \cup (RS_1 \times BS_2) \cup (BS_1 \times RS_2) \cup (BS_1 \times BS_2)$. Each of these sets is answered from different tables, depending on the combination. For example, ranges in $RS_1 \times RS_2$ are answered using $Customer\_RS\_RS$ and $Account\_RS\_RS$ table, while ranges in $BS_1 \times RS_2$ are answered using $Customer\_BS\_RS$ and $Account\_BS\_RS$ tables. 

%\subsection{Support of Equi-Join, Group By and count based Having Clause in \splitenc Scheme}
%\label{subsec:split_join}
It is sufficient to be able to join tables corresponding to the same enumeration of the BS and RS division for answering range queries. For example, $Customer\_RS\_RS$ will only be joined with $Account\_RS\_RS$ and not with any other $Account$ table. To enable this, the join columns in these two tables are encrypted with the same key.
%, further this is also sufficient to produce the correct result. For instance, in the schema shown in Figure~\ref{fig:custEnc2D}, 
For example, \emph{Customer\_RS\_RS} can be joined with \emph{Account\_RS\_RS} on the \emph{Customer\_ID} column, since same encryption key is used for this column data in both the tables.
Further, join of tables with different enumerations of BS and RS should not be allowed. For example, the join of \emph{Customer\_RS\_RS} with \emph{Account\_BS\_RS} is prevented by using different encryption keys. Otherwise, the \CRA adversary can trivially launch a binary search attack over our scheme as follows. The adversary will fix the range on the second dimension ($Balance$) to a large value so that it maps to the RS range and the binary search will be over the $Age$ dimension. These queries will mapped initially to the RS\_RS tables (when $Age$ range is large) and then to BS\_RS tables for smaller $Age$ ranges. If it were possible to join \emph{Customer\_RS\_RS} with \emph{Account\_BS\_RS}, then the tuples in \emph{Customer\_RS\_RS} can be correlated with those in \emph{Customer\_BS\_RS} using \emph{Account\_BS\_RS} (since \emph{Customer\_BS\_RS} is also joinable with \emph{Account\_BS\_RS}). This will enable the chain of binary search on $Age$ to proceed from \emph{Customer\_RS\_RS} to \emph{Customer\_BS\_RS}, allowing the adversary to break the $Age$ value accurately.

\subsubsection*{Security for MultiD Queries}
We have shown in the 1D case that is no correlation between the tuples between the BS and RS tables of a dimension, either based on position or value. In the multi-D case, there are multiple BS tables and multiple RS tables for the same dimension. We need to ensure that there is no correlation between the tuples in these tables as well. For example, there should be no correlation between tuples in \emph{Account\_RS\_RS} and \emph{Account\_BS\_RS}, both of which correspond to the RS partition of the $Balance$ dimension. If such a correlation exists, it can be used to correlate the corresponding tables \emph{Customer\_RS\_RS} and \emph{Customer\_BS\_RS}, enabling a 
binary search attack by keeping one dimension ($Balance$) fixed and searching on the other dimension as shown in the previous section. 

The $Balance$ value is encoded using same \emph{RS Encrypt} function, but with different keys in \emph{Account\_RS\_RS} and \emph{Account\_BS\_RS}. This is where the OPE/PPE encryption in the \splitenc scheme plays a role. In both these columns, the lower 16 bits are blinded using AES, so it is not possible to correlate tuples based on the lower 16 bits. However, if 
no further encryption was used, i.e. the upper 16 bits were kept as plaintext, it would be possible to correlate the tuples based on the upper 16 bits. So, further encryption is necessary and, further, that encryption should enable range queries based on the upper 16 bits to be executed on the encrypted values. Clearly, OPE and PPE are possible schemes that can be used. However, OPE by itself is not sufficient. Consider a set of values $\mathcal{V}$ encrypted using OPE with two different keys giving sets $\mathcal{V}_1$ and $\mathcal{V}_2$. Since OPE preserves order, the order of encrypted values in $\mathcal{V}_1$ and $\mathcal{V}_2$ is identical. Thus, by sorting these sets, one could correlate the values in the two sets. Similarly, PPE by itself also has an issue since PPE preserves the structure of the tree corresponding to the binary representation. In some cases, it may be possible to map nodes across two PPE trees by using the structure. For example, if in the plaintext domain, there is a single value with bit 16 as 1 and all others are have bit 16 as 0, then this value can be correlated across different PPE trees, irrespective of whether this bit gets flipped or not. The advantage of OPE is that it destroys the structure of the tree and the advantage of PPE is that is destroys the order information. Thus, by combining OPE with PPE, we can remove both order based and structure based correlation. Thus, we use a combination of OPE with PPE in the \splitenc scheme to eliminate any correlation across the multiple RS and multiple BS tables for the same dimension in the multi-D case.

\subsection{Support for Complex Queries}
%\label{subsec:split_join}
Since \splitenc scheme is a deterministic encryption scheme, it trivially supports the \emph{Group By} clause, but there is a possibility that same groups may appear as the result of different sub queries being executed on different tables. These groups can be trivially merged by the \emph{SA} after decrypting the independent results.
Similarly, count based \emph{Having} clause  can be supported in \splitenc scheme. Here also if same groups appear in the result of different sub queries executed on different tables, \emph{SA} needs to simply add the counts before returning the final result to the \emph{Client}.

%\subsection{Supporting More complicated Queries}
\label{subsec:split_extension}
Since \splitenc scheme does not return any false positives, it can be used along with other partial homomorphic encryption schemes to support a wide range of SQL queries. For example, along with the encrypted ciphertext corresponding to \splitenc scheme, we can store the \emph{paillier} encryption \cite{paillier} of plaintext data, and can support SQL queries having \emph{SUM} aggregate clause in there \emph{SELECT} statement. 
Similarly, we can store \emph{ELGAMAL} encryption \cite{elgamal} of the data in parallel with the \splitenc scheme ciphertext and allow the \emph{multiplication} of two columns in the database. In general, the SPLIT scheme can be used as a sub-system of larger systems like \emph{MONOMI} \cite{monomi} to answer range predicates in the SQL queries.

\comment{
\subsection{Application of \splitenc}
\label{subsec:app}
\splitenc can also be used to replace the \emph{OPE} scheme used in \emph{MONOMI} \cite{monomi} to answer range predicate in the SQL queries.
}

%\subsection{Implementing 2D Range Predicate Queries with Join over Encrypted Relations}
%\label{subsec:split_join_ex}
%Suppose the \emph{Client} wants to ``\emph{Count the number of customers in the bank having age between 30 and 70 and account balance between 500,000 to 5,000,000,000}''. Then it needs to issue the SQL query shown in Figure~\ref{fig:2Dquery} over the encrypted database shown in Figure~\ref{fig:custEnc2D}.
%
%\begin{figure}[h!] 
%\begin{framed}
%\emph{
%\hspace*{-2pt}{\bf SELECT} Count(*)\\
%{\bf FROM} Customer, Account \\
%{\bf WHERE} Customer.Customer\_ID = Account.Customer\_ID  \\					 
%     \hspace*{34pt} AND Customer.Age $\geq$ 30  \\ 
%     \hspace*{33pt} AND Customer.Age $\leq$ 70  \\
%     \hspace*{33pt} AND Account.Balance $\geq$ 500000  \\ 
%     \hspace*{32pt} AND Account.Balance $\leq$ 5000000000\\
%}
%\end{framed}
%\caption{SQL query to ``Count the number of customers in the bank having age between 30 and 70 and account balance between 500,000 to 5,000,000,000''}
%\label{fig:2Dquery}
%\end{figure}
%
%
%
%\begin{figure*}[h!] 
%\begin{framed}
%\begin{framed}
%\emph{
%\hspace*{-2pt}{\bf SELECT} Count(*)\\
%{\bf FROM} Customer\_BS\_BS, Account\_BS\_BS \\
%{\bf WHERE} Customer\_BS\_BS.Customer\_ID = Account\_BS\_BS.Customer\_ID  \\					 
%     \hspace*{34pt} AND Customer\_BS\_BS.Age\_BS $\geq$ 128  \\ 
%     \hspace*{33pt} AND Customer\_BS\_BS.Age\_BS $\leq$ 141  \\
%     \hspace*{33pt} AND Account\_BS\_BS.Balance\_BS $\geq$ 983040  \\ 
%     \hspace*{32pt} AND Account\_BS\_BS.Balance\_BS $\leq$ 4294967295 OR \\
%     \hspace*{33pt} AND Account\_BS\_BS.Balance\_BS $\geq$ 8589934592  \\ 
%     \hspace*{32pt} AND Account\_BS\_BS.Balance\_BS $\leq$ 9663697416 \\
%}
%\end{framed}
%\begin{framed}
%\emph{
%\hspace*{-2pt}{\bf SELECT} Count(*)\\
%{\bf FROM} Customer\_BS\_RS, Account\_BS\_RS \\
%{\bf WHERE} Customer\_BS\_RS.Customer\_ID = Account\_BS\_RS.Customer\_ID  \\					 
%     \hspace*{34pt} AND Customer\_BS\_RS.Age\_BS $\geq$ 128  \\ 
%     \hspace*{33pt} AND Customer\_BS\_RS.Age\_BS $\leq$ 141  \\
%     \hspace*{33pt} AND Account\_BS\_RS.Balance\_RS $\geq$ 4294967296  \\ 
%     \hspace*{32pt} AND Account\_BS\_RS.Balance\_RS $\leq$ 8589934591 \\
%}
%\end{framed}
%\begin{framed}
%\emph{
%\hspace*{-2pt}{\bf SELECT} Count(*)\\
%{\bf FROM} Customer\_RS\_BS, Account\_RS\_BS \\
%{\bf WHERE} Customer\_RS\_BS.Customer\_ID = Account\_RS\_BS.Customer\_ID  \\					 
%     \hspace*{34pt} AND Customer\_RS\_BS.Age\_RS $\geq$ 64  \\ 
%     \hspace*{33pt} AND Customer\_RS\_BS.Age\_RS $\leq$ 127  \\
%     \hspace*{33pt} AND Account\_BS\_BS.Balance\_BS $\geq$ 983040  \\ 
%     \hspace*{32pt} AND Account\_BS\_BS.Balance\_BS $\leq$ 4294967295 OR \\
%     \hspace*{33pt} AND Account\_BS\_BS.Balance\_BS $\geq$ 8589934592  \\ 
%     \hspace*{32pt} AND Account\_BS\_BS.Balance\_BS $\leq$ 9663697416 \\
%}
%\end{framed}
%\begin{framed}
%\emph{
%\hspace*{-2pt}{\bf SELECT} Count(*)\\
%{\bf FROM} Customer\_RS\_RS, Account\_RS\_RS \\
%{\bf WHERE} Customer\_RS\_RS.Customer\_ID = Account\_RS\_RS.Customer\_ID  \\					 
%     \hspace*{34pt} AND Customer\_RS\_RS.Age\_RS $\geq$ 64  \\ 
%     \hspace*{33pt} AND Customer\_RS\_RS.Age\_RS $\leq$ 127  \\
%     \hspace*{33pt} AND Account\_RS\_RS.Balance\_RS $\geq$ 4294967296  \\ 
%     \hspace*{32pt} AND Account\_RS\_RS.Balance\_RS $\leq$ 8589934591 \\
%}
%\end{framed}
%\end{framed}
%\caption{SQL query over encrypted database to ``Count the number of customers in the bank having age between 30 and 70 and account balance between 500,000 to 5,000,000,000''}
%\label{fig:BSRS2Dquery}
%\end{figure*}
%
%Suppose our underlying encryption scheme is \emph{OPE}. The domain of Age column is $[0,128]$ and the domain of \emph{Balance} column is complete  \emph{32-bit} integers. Since we have used \emph{OPE} the domain of the ciphertext tree will be exactly double, hence encrypted value of age column will lie in range $[0,255]$ while that of \emph{Balance} will be complete \emph{64-bit} integers. Lets consider \emph{DO} has kept levels $L_0$ to $L_3$ into the BS division and levels $L_4$ to $L_8$ into the RS division for \emph{Age} columns, while for \emph{Balance} column \emph{DO} has  selected levels $L_0$ to $L_{31}$ into the BS division while levels $L_{32}$ to $L_{64}$ into RS division.
%
%Say, while computing the OPE for the constants corresponding to the \emph{Age} column, we get OPE(30) = 64 and OPE(70) = 141, then according to the selected partitioning of the BS and RS levels for this column, the subrange $[64,127]$ will lie in the RS division while $[128,141]$ will lie in the BS division. Similar computations for the balance column is done in Section~\ref{subsec:split_id_ex}.
%
%Now query evaluation will proceed similarly as described in Section~\ref{subsec:split_id_ex}, except in Step 3, instead of two queries, \emph{four} queries will be issued by \emph{SA} to the \emph{SP}. The four queries are shown in Figure~\ref{fig:BSRS2Dquery}. Further since, these \emph{four} queries are for four different tables and will fetch disjoint set of rows, they can be executed in parallel.
%
%
%\subsection{* Possible Attack against \splitenc}
%\label{subsec:split_attack}
%If the \adv adversary selects one range at granularity of $2^{32}$, then it can decrypt any value in that range using the binary search over $BS$ table.



\subsection{Enhancements to \splitenc}
\label{subsec:opt}
In this section we will show how we can further improve the efficiency and security of the \splitenc scheme. \comment{\emph{reduce the required storage overhead} for the MultiD case, how we can \emph{enhance the overall security}, and how we can \emph{reduce the query execution time} in \splitenc scheme. }

\subsubsection*{Partitioning of Columns} 
Let $Col$ represent the set comprising of all the columns in the database. If the DO can identify a subset of columns $S$ that will not be used in any query, then they need not be considered for the \splitenc scheme at all. Only the set $C = Col - S$ need to be considered. Further, 
\comment{
, then if the \emph{Data Owner (DO)} is able to:
\begin{enumerate}
\item Partition \{\emph{Col}\} into two disjoint sets, such that \{$Set_1$\} is the set of database columns that will not be used as range predicate in any legal SQL query, and \{$Set_2$\} is the set of all the columns on which range predicates can occur, formally
\begin{itemize}
\item \{$Set_1$\} $\cap$ \{$Set2$\} $ = \phi$
\item \{$Set_1$\} $\cup$ \{$Set_2$\} $ = $ \{\emph{Col}\}
\end{itemize}

\item }
suppose that \emph{DO} can partition $C$ into a number of equivalence classes, such that columns in a equivalence class cannot occur simultaneously as range predicates in any query. Say \emph{DO} is able to identify \emph{d} equivalence classes $C_1, C_2, \cdots C_d$.
\comment{
\begin{itemize}
\item \{$Set_{2i}$\} $\cap$ \{$Set_{2j}$\} $=$ $\phi$  $~~~~~~ \forall (i,j) \in \{1, ..., d\}$ 
\item \{$Set_{21}$\} $\cup$ \{$Set_{22}$\} $\cup ... \cup$ \{$Set_{2d}$\} $=$ \{$Set_2$\}
\end{itemize}
Since only the columns in \{$Set_2$\} participate in a range predicate and are further restricted to be one from each equivalence class at a time, hence }
The maximum number of dimensions allowed in any legal query on this database will be $d$. Hence, the total blowup due to table replication will be $T * 2^d$ instead of $T*2^n$.
%\end{enumerate}
\begin{figure}[h!] 
\begin{framed}
\small
\emph{{\bf Table1}(ColA, ColB, ColC)}\\
\emph{{\bf Table2}(ColD, ColE, ColF)}\\
\emph{{\bf Table3}(ColG, ColH, ColI)}
\end{framed}
\caption{Test Database Schema}
\label{fig:testschema}
\end{figure}
For example, consider a test database shown in Figure~\ref{fig:testschema} with $Col = \{ColA, ColB, ColC, ColD, ColE, ColF, ColG, ColH, ColI\}$
If the database has to support simultaneous range predicates over all the columns, then the total number of tables in the encrypted database would be $3 * 2^9=1536$.
Now suppose $S = \{ColA, ColB, ColD, ColG, ColI\}$, which implies that $C= \{ColC, ColE, ColF, ColH\}$. Further, say $C$ can be partitioned into two equivalence classes, namely, $C_1 = \{ColC, ColH\}$ and $C_2 = \{ColE, ColF\}$. Then the total number of tables in the encrypted database is $3 * 2^2=12$ as shown in Figure~\ref{fig:enctestschema}.

\begin{figure}[h!] 
\begin{framed}
\small
\emph{{\bf Table1\_BS\_BS}(ColA, ColB, ColC\_BS)}\\
\emph{{\bf Table2\_BS\_BS}(ColD, ColE\_BS, ColF\_BS)}\\
\emph{{\bf Table3\_BS\_BS}(ColG, ColH\_BS, ColI)}\\

\emph{{\bf Table1\_BS\_RS}(ColA, ColB, ColC\_BS)}\\
\emph{{\bf Table2\_BS\_RS}(ColD, ColE\_RS, ColF\_RS)}\\
\emph{{\bf Table3\_BS\_RS}(ColG, ColH\_BS, ColI)}\\

\emph{{\bf Table1\_RS\_BS}(ColA, ColB, ColC\_RS)}\\
\emph{{\bf Table2\_RS\_BS}(ColD, ColE\_BS, ColF\_BS)}\\
\emph{{\bf Table3\_RS\_BS}(ColG, ColH\_RS, ColI)}\\

\emph{{\bf Table1\_RS\_RS}(ColA, ColB, ColC\_RS)}\\
\emph{{\bf Table2\_RS\_RS}(ColD, ColE\_RS, ColF\_RS)}\\
\emph{{\bf Table3\_RS\_RS}(ColG, ColH\_RS, ColI)}\\
\end{framed}
\caption{Encrypted Test Database Schema}
\label{fig:enctestschema}
\end{figure}

\comment{
For a real example, we can look at the TPCH benchmark \cite{tpch} database and the recommended \emph{22} benchmarking queries. Total number tables in TPCH database is \emph{8} and total number of columns in the database is \emph{61} but only \emph{4} columns take part as the range predicate, i.e. \{$|Set_1|$\} $=$ \emph{57}, and \{$|Set_2|$\} $=$ \emph{4}. Namely, \{$Set_2$\} $=$ \{\emph{O\_ORDERDATE, L\_SHIPDATE, L\_RECEIPTDATE, L\_QUANTITY}\}. Further we can split \{$Set_2$\} into two groups, viz. \{$Set_{21}$\} $=$ \emph{O\_ORDERDATE} and \{$Set_{22}$\} $=$ \emph{L\_SHIPDATE, L\_RECEIPTDATE, L\_QUANTITY}. Thus total number of tables in the encrypted TPCH database is $8 * 2^2$, i.e. the encrypted database is \emph{four} times the size of the plaintext database.
}

\subsubsection*{Enhancing the security of \splitenc} 
In the \splitenc scheme, we create two ciphertexts for every plaintext data, by dividing the set of levels in the tree into two (RS and BS) divisions, and keep them in two different tables. One could take this to an extreme and create a division out of each level in the tree. This would prevent a binary search across levels. Due to space constraints, we provide the following results without proof. If the data consists of $n$ bit numbers and number of levels in the RS and BS divisions is $r$ and $b$ respectively ($r + b = n$), then the number of queries $q$ required to map a data item to a range of size $2^m$ is given by the following formula:
\[
q = 
\begin{cases}
2^r + (b - m), & \text{if } b > m \\
n - m, & \text{otherwise}
\end{cases}
\]
On the other hand, if each level is a separate division and stored in a separate table, then the number of queries required for the same level of breach is $2^{n - m}$. It can be seen that this is always larger than the number of queries with two divisions, thus increasing the security. The flip side is that the space overhead becomes $T * n^d$, which is impractical even for small $d$. However, having an intermediate approach of using more than 2 divisions can definitely be considered.

\comment{
Till now in \splitenc scheme we have produced two ciphertext for every plaintext data and kept them in two different tables corresponding to the BS and RS divisions, further since we are building upon \emph{OPE and PPE} schemes, thus over half of the bits of any ciphertext one of the attack shown in Section~\ref{sec:existingSolns} can be mounted. But on the other hand, if we increase the number of ciphertexts corresponding to every plaintext, for example, we can keep each level of the ciphertext tree into its own table instead of dividing them into two groups, i.e. if plaintext data is represented using \emph{64-bits}, we will have \emph{64} corresponding ciphertexts, and hence every plaintext table will have \emph{64} corresponding ciphertext table. Thus in this case we can show that \adv adversary will not be able to mount any of the attacks shown in Section~\ref{subsec:attackModels} over any of the \emph{64} ciphertext tables.
}
\subsubsection*{Reducing the query execution time} In \splitenc scheme, each plaintext table is replicated into multiple tables, and each plaintext query is translated into multiple queries over ciphertext tables. However, it can be observed that each ciphertext query accesses a different table and further, it accesses a disjoint set of rows from each table. 
\comment{But key observation is that, 
\begin{itemize}
\item Each ciphertext query access disjoint set of rows, and
\item Each ciphertext query access independent tables.
\end{itemize}
}
Hence, these queries can be executed in parallel and their outcome can be merged by the \emph{SA} with simple addition (in case of count as query result) or union (in case of tuples as query result) operations.

\subsection{Limitations of \splitenc}
\label{subsec:split_limits}
The \splitenc scheme does have some limitations, which should be kept in mind. The storage space required by \splitenc scheme is exponential in the number of equivalence classes of the database columns on which range predicate queries are permitted. However, this may not be too much of a problem since storage is getting cheaper with time. Another limitation is that only batch updates can be allowed to a ciphertext database encrypted using the \splitenc encryption scheme. This requirement is essential to prevent correlation between the encrypted database tables based on the sequence of inserts.


