\section{Introduction}
\label{sec:introduction}
Cloud computing has led to the emergence of the ``Database-as-a-Service'' (DBaaS) model for outsourcing databases to the Cloud. The cost advantage and flexibility of the DBaaS model makes it attractive for enterprises to outsource their data and query processing activities to third-party service providers such as Amazon RDS~\cite{amazon}and Microsoft Azure SQL Database~\cite{azure}\comment{ and IBM Cloudant~\cite{cloudant}}. However, security of confidential data hosted in remote locations is a major concern for enterprises looking to outsource their data. A straightforward way to protect data is to encrypt it before uploading to the Cloud. 
However, simple encryption does not directly support query processing in the encrypted domain, forcing clients to download and decrypt the entire data for executing any query. The net effect is that the Cloud is reduced to a storage repository, and ceases to be a first-class data processing engine in its own right. To address this, a lot of effort has been put in devising encryption mechanisms that enable query processing directly on encrypted data without compromising on its security. 


To prove the security of such schemes, one has to define an adversarial model that fixes the objectives and powers of the adversary. The adversary could be any entity in the system that is not trusted or can be compromised.
\comment{
In this paper we look at the problem of processing range predicates in SQL queries over encrypted cloud databases.
Our technique to process range predicates securely and efficiently can also be used to handle equality predicates, group by and equi-join operators. Queries containing range predicates, equality predicates, equi-joins of relations and grouping are very common in both transaction systems as well as analytical systems. Hence it is evident that secure and efficient processing of range predicates in SQL queries is very critical for building a secure Database-as-a-Service system.}
%System entities figure
A typical setup in the DBaaS model consisting of \emph{four} entities is shown in Figure~\ref{fig:entities}. This includes the \emph{Service Provider (SP)}, which provides the cloud infrastructure, the \emph{Data Owner (DO)} who owns the data, \emph{Clients (C)} who are authorized to issue queries on data stored by \emph{DO} at \emph{SP}'s infrastructure and the \emph{Security Agent (SA)}, which acts as the bridge between \emph{DO} and \emph{SP} or \emph{C} and \emph{SP}. 
For example, a bank (DO) stores its data on Amazon RDS (SP). Third parties, such as auditors or other service providers, that perform analysis over bank's database will serve as the clients (C) in the system. The \emph{SA} is a transparent entity that helps to translate the plaintext queries into the corresponding query in the ciphertext space.
\begin{figure}[H]
\includegraphics[width=\linewidth]{entities_new.png}
\vspace*{-20pt}
\caption{System Entities}
\label{fig:entities}
\end{figure}
\vspace*{-5pt}

\comment{The adversarial model which describes the powers and objective of an adversary against the system is very closely related to these entities. Any entity which is inherently not trusted or which can be compromised adds to the power of the adversary. In our cloud computing model,} The Data Owner and the Security Agent are considered trusted,  while the Service Provider is always untrusted. We assume that the adversary is only interested in learning the plaintext data and not in affecting the function of the database. Thus the database engine is not corrupted and answers queries honestly and correctly. The Clients can either be trusted or un-trusted. This gives rise to two adversarial models: a) \emph{\hbc} (\HBC) model, in which the client is trusted and only Service Provider can be compromised and b) \emph{\adv} (\HBA) model, in which the client is not trusted and can also \emph{collude} with the Service Provider. In the \HBC model, only passive attacks are possible. The service provider can try to learn actual values of data by observing the encrypted data (\coa - \COA) and optionally by knowing the ciphertext mapping for a few fixed plaintexts (\kpa - \KPA). The \HBA model, on the other hand, is very powerful and enables active attacks. The adversary can adaptively issue queries by acting as a Client and observe how the query is processed at the service provider's site to learn the encryptions of the values returned in the query results. If only equality predicates are allowed in the queries, it leads to a \cpa (\CPA), whereas for range queries it leads to the \cra (\CRA). 

All the research till now has focused on the \HBC model and largely ignored the \HBA model. However, a \HBA adversary is quite plausible as can be seen from the following example. Consider a bank that has outsourced its data consisting of the Customer and Account tables to the Cloud. The schema for these tables is shown in Figure~\ref{fig:custschema}. The bank may need to provide an interface to third parties, such as auditors, external analysts, or the banks customers, to query the data. Typically, such interfaces are form based, in which the Clients enter some parameters and the query get generated automatically. For example, there could be a form for a report that counts the number of customers in a given age range ($a_l:a_h$) and having account balance in a specified range ($b_l:b_h$). The corresponding query that gets generated is shown in Figure~\ref{fig:formQuery}. By varying the parameters in the form, a colluding Service Provider and Client can easily launch a \CRA attack.

\comment{
%Insert real life example here
For example, consider there is a Bank (say $B$) whose database schema for the customer section is shown in Figure~\ref{fig:custschema}.}
\begin{figure}[h!] 
\begin{framed}
\small
\emph{{\bf Customer}(Customer\_ID, Age)}\\
\emph{{\bf Account}(Customer\_ID, Branch\_ID, Balance)}
\end{framed}
\vspace*{-15pt}
\caption{Customer Database Schema}
\label{fig:custschema}
\end{figure}
\vspace*{-13pt}
\comment{
Now suppose the Client (analyst) wants to analyze the relation between the age and the account balances of the customers, it may ask following query, ``\emph{Count the number of customers in each branch having age between 30 and 70 and account balance between 5,000 to 5,000,000}''. So to retrieve the required information the analyst would like to issue below SQL query to the customer database:
}
\begin{figure}[h!] 
\begin{framed}
\small
\emph{
\hspace*{-2pt}{\bf SELECT} Branch\_ID, Count(*)\\
{\bf FROM} Customer, Account \\
{\bf WHERE} Customer.Customer\_ID = Account.Customer\_ID  \\					 
     \hspace*{14pt} AND Customer.Age BETWEEN $a_l$ AND  $a_h$\\ 
     \hspace*{13pt} AND Account.Balance BETWEEN $b_l$ AND $b_h$;\\ 
\vspace*{-10pt}     
%{\bf GROUP BY} Branch\_ID;\\
}
\end{framed}
\vspace*{-15pt}
\caption{Form based SQL query}
\label{fig:formQuery}
\end{figure}
\vspace*{-5pt}

As with several other papers, our primary focus is on the secure processing of \emph{Range Queries} over encrypted databases, which are the core building blocks of decision-support queries. \comment{Form based interfaces also typically generate range queries on the base data and not on the intermediate results of other operations.} Techniques for range queries are generally applicable to equality predicates, group-by and equi-join operators as well, making them critical for building secure DBaaS systems.


The state of the art in secure processing of range queries consists of the OPE~\cite{openum,opebold,boldy12,opepopa}, PPE~\cite{ppe,ppetree}, SDB~\cite{sdb} and PBTree~\cite{pbtree} schemes. The security of these schemes have been analysed only in  \HBC model. However, they are easily breached using the \cra in the \HBA model using only $log_2(N)$ adaptive queries, where $N$ is the plaintext domain size. To address this gap, we present a new encryption scheme, called \splitenc, for the secure and efficient handling of range predicates in \HBA environments. We show that \splitenc is more secure than existing schemes; the adversary can only map a target ciphertext to a range of size $\sqrt{N}$ in $log_2(N)$ queries and requires $\sqrt{N}$ adaptive queries to completely decrypt a target ciphertext. A comparison of the security of \splitenc with existing schemes under various attacks is shown in Figure~\ref{fig:seccomp}.

\begin{figure}[H] 
\includegraphics[width=\linewidth]{security.png}
\vspace*{-20pt}
\caption{Security of Various Schemes}
\label{fig:seccomp}
\end{figure}
\vspace*{-5pt}

\comment{
Writing above SQL query requires every analyst to be familiar with the bank's database schema and SQL syntax, which is not always the case. 
So the bank usually develops an application which exposes a form based interface for the analysts to vary the parameters, issue SQL queries and receive the plaintext result. Now if this Client colludes with \emph{SP}, then \emph{HBA} adversary can issue above SQL query for the parameters of its choice, through the colluded Client.
}

For the security analysis, we consider the objective of the adversary to be \emph{Data Range Recovery}.  \comment{The intuition behind this is that adversary sees a lot of encrypted data being stored at the server.} The adversary chooses a particular encrypted data item (how he chooses is immaterial) and then tries to identify the interval in which its plaintext value lies. An interval of size 1 implies that the adversary has identified the plaintext exactly. 
\comment{
While trying to decrypt this ciphertext value he may learn plaintext values of some other ciphertext but that is not considered a high threat, since the ciphertext being decrypted is not in his control.}

\comment{
As with several other papers, our primary focus in on the secure processing of \emph{Range Queries} over encrypted databases, which are the core building blocks of decision-support queries. \comment{Form based interfaces also typically generate range queries on the base data and not on the intermediate results of other operations.} Techniques for range queries are generally applicable to equality predicates, group-by and equi-join operators as well, making them critical for building secure DBaaS systems. For complex queries that contain range predicates on the input tables, techniques proposed by MONOMI~\cite{monomi} can be used in combination with \splitenc for the range predicates.
}

\comment{
The state of the art in secure processing of range queries consists of the OPE and PPE schemes. These schemes are shown to be secure in the \HBC model. However, they are easily breached in the \HBA model using only $log_2(N)$ adaptive queries, where $N$ is the plaintext domain size. In fact, none of the schemes proposed till date have been analyzed in the \HBA model. As we show in Section~\ref{subsec:existingSolns}, the \HBA model is a very plausible model and DBaaS systems of the future should aim to be secure in this model. To address this gap, we present a new encryption scheme, called \splitenc, for the secure and efficient handling of range predicates in \HBA environments.
}
\comment{
\subsection{Our Contribution}
\label{subsec:contribution}
In this paper we propose a new encryption scheme, which uses an \emph{order-preserving encryption} scheme and a \emph{prefix-preserving encryption} scheme and offers more security, in the sense that any \adv adversary will not be able to compute the plaintext from its corresponding ciphertext despite of its adaptive querying power. }

The main challenge in the \CRA model for any encryption scheme that supports range predicate processing is that the adversary can launch a binary search attack. A binary search is possible since all the bits of the ciphertext are stored together. To avoid this, the 
\splitenc scheme splits the plaintext into two parts,
and separately encrypts these parts, using a composition of PPE and OPE,
to produce a pair of ciphertexts that are then stored in separate database
tables. Any association between the tuples of two ciphertext tables is also removed. This breaks the chain of queries in a binary search, making the scheme much more secure. 
\comment{Specifically, we show that if $N$ is the plaintext domain size, then an adversary needs at least $\sqrt{N}$ adaptive queries to decrypt any ciphertext encrypted using the \splitenc scheme.}

\comment{
The OPE or PPE can be visualized using a complete binary tree over the ciphertext domain.
Each encrypted value stores the whole path information from root of this ciphertext tree to the leaf node representing the input data. This property of having all the information in a single place leads to the possibility of binary search over the ciphertext domain. In our work we split the information of every plaintext into two parts. Geometrically what we do it to divide all the levels in the complete binary tree representing the plaintext into two contiguous parts, one part corresponds to the contiguous top levels of the tree while the other corresponds to the contiguous bottom levels of the tree. While encrypting a single data value we produce two ciphertexts. These two ciphertexts are then stored separately in different tables. Any association between the tuples of two ciphertext tables is also removed. This makes our scheme secure in \adv model.  We call this scheme \emph{\splitenc}. The scheme and its details are explained in Section~\ref{sec:split_enc}.
}

In the \splitenc scheme, at query processing time, each range predicate is rewritten into
an equivalent set of table-specific sub-range predicates, and the union of
their results forms the query answer. Since each user query results in multiple queries over multiple tables, there is some additional overhead of the \splitenc scheme. We conducted a
detailed evaluation of \splitenc on benchmark databases and demonstrate that its
execution times are within a factor of $2$ of the corresponding plaintext
times. Thus, it is able to provide a much higher level of security at a very small overhead. Further, the \splitenc scheme can be implemented on top of existing database engines, making it very convenient to use in current DBaaS solutions. 

\comment{
\subsection{Organization*}
\label{subsec:org}
}
The rest of the paper is organized as follows. We first introduce some preliminaries in Section \ref{sec:prelim}. Section \ref{sec:problemfw} introduces our problem framework and the attack model. 
%Section \ref{sec:existingSolns} shows attacks against existing solutions in the \HBA attack model.
Section \ref{sec:split_enc} describes our basic \splitenc encryption scheme and its security. 
Section \ref{sec:extension} proposes extension to our scheme to multi-dimensional range predicate queries,
Section \ref{sec:exp} provides the experimental results, Section \ref{sec:relwork} reviews the related work and
Section \ref{sec:conc} concludes the paper and describes the future work.

\comment{
\subsection{Background Review}
\label{subsec:background}
Numerous attempts have been made to devise secure schemes to answer range predicates over encrypted databases. In the \emph{Bucketing Scheme}~\cite{sqlindas,ppindex}, the whole data domain is partitioned into buckets, each of which has a unique ID. This ID is stored at the server along with encrypted data. The input range query is mapped to bucket ids and all the rows which fall into these buckets are returned. This scheme leads to false positives if the bucket boundary does not match with range boundary. 
In \emph{OPE Schemes}~\cite{openum,opebold,boldy12,opepopa}, the encryption function preserves the order of the plaintext i.e. if $a \le b$ then $E(a) \le E(b)$.  Any range predicate over plaintext can be easily converted into a range predicate over encrypted values. Various implementations of order preserving encryptions have been proposed but they are not secure in the \HBA model. As described earlier, an adversary in the \HBA model can perform a binary search over the domain and successfully decrypt any target encrypted value.
The \emph{PPE scheme}~\cite{ppe,ppetree}  preserves prefix based clustering, i.e. values in the plaintext domain that have a common prefix have a common prefix in the encrypted domain as well. This preserves the querying ability on the prefix of the input data; any range predicate on plaintext data is converted into equality queries over prefixes. This encryption scheme is also not secure in the \HBA model, due to a similar binary search attack.  
There are other solutions~\cite{pbtree} that work towards providing a new index structure to handle range predicates in a secure manner. However, they require a significant changes to the underlying database systems which hinders their adoption by the industry. Further, they are not analyzed in the \HBA adversarial model.
Recently, Wong et. at. \cite{sdb} have came up with a new encryption scheme called SDB to securely process SQL queries over a encrypted database. However, this scheme can be shown to be insecure in the \HBC model itself, as shown in Section \ref{sec:sdbAttack}.
}
\comment{
In Section~\ref{sec:existingSolns} we will give examples to specifically show the weaknesses of OPE and PPE encryption scheme against \adv adversary. }
