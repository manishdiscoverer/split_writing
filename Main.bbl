\begin{thebibliography}{10}

\bibitem{amazon}
\url{http://aws.amazon.com/}.

\bibitem{azure}
\url{http://azure.microsoft.com/en-in/services/sql-database/}.

\bibitem{openum}
Rakesh Agrawal, Jerry Kiernan, Ramakrishnan Srikant, and Yirong Xu.
\newblock Order preserving encryption for numeric data.
\newblock In {\em Proceedings of the 2004 ACM SIGMOD international conference
  on Management of data}, pages 563--574. ACM, 2004.

\bibitem{cipherbase}
Arvind Arasu, Spyros Blanas, Ken Eguro, Raghav Kaushik, Donald Kossmann,
  Ravishankar Ramamurthy, and Ramarathnam Venkatesan.
\newblock Orthogonal security with cipherbase.
\newblock In {\em CIDR}, 2013.

\bibitem{trusteddb}
Sumeet Bajaj and Radu Sion.
\newblock Trusteddb: A trusted hardware based outsourced database engine.
\newblock In {\em VLDB, DEMO}, 2011.

\bibitem{fpe}
Mihir Bellare, Thomas Ristenpart, Phillip Rogaway, and Till Stegers.
\newblock Format-preserving encryption.
\newblock In {\em Selected Areas in Cryptography}, pages 295--312. Springer,
  2009.

\bibitem{opebold}
Alexandra Boldyreva, Nathan Chenette, Younho Lee, and Adam O'neill.
\newblock Order-preserving symmetric encryption.
\newblock In {\em Advances in Cryptology-EUROCRYPT 2009}, pages 224--241.
  Springer, 2009.

\bibitem{boldy12}
Alexandra Boldyreva, Nathan Chenette, and Adam O'Neill.
\newblock Order-preserving encryption revisited: Improved security analysis and
  alternative solutions.
\newblock In {\em Advances in Cryptology--CRYPTO 2011}, pages 578--595.
  Springer, 2011.

\bibitem{elgamal}
Taher ElGamal.
\newblock A public key cryptosystem and a signature scheme based on discrete
  logarithms.
\newblock In {\em Advances in cryptology}, pages 10--18. Springer, 1985.

\bibitem{sqlindas}
Hakan Hacig{\"u}m{\"u}{\c{s}}, Bala Iyer, Chen Li, and Sharad Mehrotra.
\newblock Executing sql over encrypted data in the database-service-provider
  model.
\newblock In {\em Proceedings of the 2002 ACM SIGMOD international conference
  on Management of data}, pages 216--227. ACM, 2002.

\bibitem{ppindex}
Bijit Hore, Sharad Mehrotra, and Gene Tsudik.
\newblock A privacy-preserving index for range queries.
\newblock In {\em Proceedings of the Thirtieth international conference on Very
  large data bases-Volume 30}, pages 720--731. VLDB Endowment, 2004.

\bibitem{kl}
Jonathan Katz and Yehuda Lindell.
\newblock {\em Introduction to modern cryptography}.
\newblock CRC Press, 2014.

\bibitem{ppetree}
Jun Li and Edward~R Omiecinski.
\newblock Efficiency and security trade-off in supporting range queries on
  encrypted databases.
\newblock {\em DBSec}, 5:69--83, 2005.

\bibitem{pbtree}
Rui Li, Alex~X Liu, Ann~L Wang, and Bezawada Bruhadeshwar.
\newblock Fast range query processing with strong privacy protection for cloud
  computing.
\newblock {\em Proceedings of the VLDB Endowment}, 7(14):1953--1964, 2014.

\bibitem{tls}
Athar Mahboob and Nassar Ikram.
\newblock Transport layer security (tls)--a network security protocol for
  e-commerce.
\newblock Technical report, Technical report, National University of Science \&
  Technology. http://www. researchgate.
  net/publication/216485703\_Transport\_Layer\_Security\_ (TLS)--A\_Network\_
  Security\_Protocol\_for\_E-commerce/file/79e415093d5e82cbb2. pdf.

\bibitem{handbook}
Alfred~J Menezes, Paul~C Van~Oorschot, and Scott~A Vanstone.
\newblock {\em Handbook of applied cryptography}.
\newblock CRC press, 1996.

\bibitem{paillier}
Pascal Paillier.
\newblock Public-key cryptosystems based on composite degree residuosity
  classes.
\newblock In {\em Advances in cryptology-EUROCRYPT'99}, pages 223--238.
  Springer, 1999.

\bibitem{opepopa}
Raluca~A Popa, Frank~H Li, and Nickolai Zeldovich.
\newblock An ideal-security protocol for order-preserving encoding.
\newblock In {\em Security and Privacy (SP), 2013 IEEE Symposium on}, pages
  463--477. IEEE, 2013.

\bibitem{cryptDB}
Raluca~Ada Popa, Catherine Redfield, Nickolai Zeldovich, and Hari Balakrishnan.
\newblock Cryptdb: Processing queries on an encrypted database.
\newblock {\em Communications of the ACM}, 55(9):103--111, 2012.

\bibitem{monomi}
Stephen Tu, M~Frans Kaashoek, Samuel Madden, and Nickolai Zeldovich.
\newblock Processing analytical queries over encrypted data.
\newblock In {\em Proceedings of the VLDB Endowment}, volume~6, pages 289--300.
  VLDB Endowment, 2013.

\bibitem{sdb}
Wai~Kit Wong, Ben Kao, David Wai~Lok Cheung, Rongbin Li, and Siu~Ming Yiu.
\newblock Secure query processing with data interoperability in a cloud
  database environment.
\newblock In {\em Proceedings of the 2014 ACM SIGMOD international conference
  on Management of data}, pages 1395--1406. ACM, 2014.

\bibitem{ppe}
Jun Xu, Jinliang Fan, Mostafa~H Ammar, and Sue~B Moon.
\newblock Prefix-preserving ip address anonymization: Measurement-based
  security evaluation and a new cryptography-based scheme.
\newblock In {\em Network Protocols, 2002. Proceedings. 10th IEEE International
  Conference on}, pages 280--289. IEEE, 2002.

\end{thebibliography}
