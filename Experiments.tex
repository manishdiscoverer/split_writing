\section{Experimental Evaluation}
\label{sec:exp}

\subsection{Experimental Setup}
\label{subsec:exp_setup}

In this section, we present the experimental results for range query processing using \splitenc encryption scheme. The experimental setup consists of two machines, each with 8 Intel Xeon 2.0 GHz processors, 32 GB  RAM, 500 GB HDD running Windows server 2008 service pack 2. One of the machine acts as the server and other acts as the client. Postgresql 9.4 was used as the database engine. The working memory of database was set to be 4 GB. Before running each query, the database was restarted to avoid the effects of database caching across queries. Further, a large 32 GB file was scanned to flush the OS cache before each query. All the queries were issued using a java program. The java program also converted the queries over plain text domain to queries over encrypted domain.


\subsection{Database Setup}
\label{subsec:exp_db_set_setup}

Experiments were carried out on TPCH benchmark database of various sizes (5 GB and 10 GB). The number of dimensions in the query is an important factor that affects the \splitenc scheme. To study this, both the databases were encrypted using the \splitenc scheme for 1-D, 2-D, 3-D and 4-D queries. Hence a total to 10 separate databases (including plaintext database) were created.
Non-clustered indexes were created on columns which were used in the queries. These indexes were created on both the plain text database as well as the encrypted database for providing a fair comparison.

All the negative numbers were converted to positive numbers by shifting. Also, all the fractions were rounded up to the nearest integer. These were done in the plain text tables also for providing a fair comparison and to ensure that we could validate that encrypted queries result set.
All the string fields were encrypted using AES encryption.
To all the tables (plain and encrypted) we added a column called IND of type integer, which always contains a constant value 1.
The range queries which we gave were of the form 
``\textit{select sum (IND) from table where <range condition>}''.
This query forces the database to fetch the actual record satisfying the predicates from the disk and prevents it from answering the query using the index itself.

\subsection{Results}
We measured the overheads of the \splitenc scheme w.r.t. the plaintext database, in terms of the extra storage required and the extra time needed during query processing.
\subsubsection*{Storage Overhead}
\label{subsec:exp_data_blowup}

%The first step in SPLIT is to encrypt the plain text database. Figure \ref{fig:exp_data_ingest_time} shows the time taken to ingest database of various sizes. <<to be done>>

Figure \ref{fig:exp_database_size} shows the size of database (in GB) on disk. The figure shows that for handling queries of $d$ dimensions the database size blows up by $2^d$, which is expected. This trade off is necessary for getting more security against the \HBA adversary. Since the number of dimensions in a query is typically small (less than 3 in the TPCH queries) and since disk is cheap, this overhead is not a big concern.

%\begin{figure}[H]
%\includegraphics[width=\linewidth]{ingest_time.png}
%\caption{Time taken for creating database. All the times are in seconds.}
%\label{fig:exp_data_ingest_time}
%\end{figure}

\begin{figure}[H]
\includegraphics[width=\linewidth]{database_size.png}
\caption{Size of various databases on disk. \comment{All the sizes are in GB.}}
\label{fig:exp_database_size}
\end{figure}

\subsubsection*{Query Processing Overhead}
\label{subsec:query_processing_exp}

Queries were given on lineitem table which is the largest table in TPCH. The queries were given on the following dimensions, $l\_orderkey$ (dimension 1), $l\_shipdate$ (dimension 2), $l\_partkey$ (dimension 3) and $l\_receiptdate$ (dimension 4). For 1-D case only dimension 1 was used, for 2-D case dimension 1 \& 2 were used and so on. The selectivity of each dimension was varied from $10$ percent to $100$ percent independently and, in each query, all dimensions had the same selectivity.

As seen in Section~\ref{sec:split_enc}, one plain text query range gets converted into multiple non-continuous ranges over encrypted domain.
Each of these encrypted range goes to either a BS table or RS table.
So, one encrypted table can receive multiple of these mapped non continuous ranges when executing a range query.
We union all these ranges together into a single query over the encrypted table.
For multi-dimensional query processing we can have a query per encrypted table, that means we can have atmost $2^d$ queries.
We union all these queries into a single query and send it to the server in one go.

\begin{figure}[H]
\includegraphics[width=\linewidth]{running_time_5gb.png}
\caption{Query running time for 5 GB TPCH database.}
\label{fig:exp_qp_5gb}
\end{figure}

\begin{figure}[H]
\includegraphics[width=\linewidth]{running_time_10gb.png}
\caption{Query running time for 10 GB TPCH database.}
\label{fig:exp_qp_10gb}
\end{figure}

Figures \ref{fig:exp_qp_5gb} and \ref{fig:exp_qp_10gb} show the time taken for query processing. We varied the number of dimensions in the query as well as the selectivity of each dimension. The black portion of the bar represents the time taken to execute the plain text query and grey portion represents the overhead incurred in encrypted query processing.
The figures show that the query overhead of the \splitenc scheme increases as we increase the number of dimensions. This is expected, since the number of sub-queries generated by the range mapping algorithm and the number of tables against which these queries run both grow with the number of dimensions. However, this overhead seems to plateau out, and even in 10 GB 4-D case we are able to perform within \emph{2x} of the plain text database. This is due to the fact that though the number of sub-queries is large, each sub-query accesses a disjoint set of tuples. Thus the total work done is almost equivalent to that of the single query in the plaintext domain, particularly if indexes are used in the query plan. This points to the practicality of the \splitenc scheme. Also, the fact that \splitenc does not need any change to existing databases means that it can be used with existing database engines.

The \splitenc needs query re-writing as the first step of query execution. During experiments we found the time taken for this re-writing to be 10 ms in worst case. This cost does not change much with the number of dimensions and hence is insignificant as compared to the query execution time.

One thing to note here is that all these experiments were done without any parallelization. For each plain text query, we get multiple cipher text queries (each being over a different table). We ran these queries sequentially, but these can be executed in parallel to improve the efficiency of \splitenc.
These results show that the \splitenc scheme maintains good performance (within 2x) while providing good security.