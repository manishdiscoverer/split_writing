\section{Real Life Scenario}
\label{sec:reality}
This section motivates the \adv adversary by providing a real life scenario. This scenario shows that \adv model is very much possible in real world and a system which is secure in this model should be used instead of a system which is secure in weaker \hbc model.

Consider there is a Bank (say $B$) whose database schema for the customer section is shown in Figure~\ref{fig:custschema}. The database schema shown is just a part of the original database design. This is done to present neat examples in restricted space. 
\begin{figure}[h!] 
\begin{framed}
\small
\hspace*{-2pt}\emph{{\bf Customer}(Customer\_ID, Age)}\\
\emph{{\bf Account}(Customer\_ID, Branch\_ID, Balance)}
\end{framed}
\caption{Customer Database Schema}
\label{fig:custschema}
\end{figure}
Now suppose, in order to make new policies, the customer section of the bank wants to analyze the relation between the age and the account balances of the customers, so in order to infer knowledge they may like to count the number of customers  in various age and account balance buckets, for instance they may ask following query, ``\emph{Count the number of customers in each branch having age between 30 and 70 and account balance between 500,000 to 5,000,000,000}''. So to retrieve the required information the analyst would like to issue below SQL query to the customer database:
\begin{figure}[h!] 
\begin{framed}
\small
\emph{
\hspace*{-2pt}{\bf SELECT} Branch\_ID, Count(*)\\
{\bf FROM} Customer, Account \\
{\bf WHERE} Customer.Customer\_ID = Account.Customer\_ID  \\					 
     \hspace*{34pt} AND Customer.Age $\geq$ 30  \\ 
     \hspace*{33pt} AND Customer.Age $\leq$ 70  \\
     \hspace*{33pt} AND Account.Balance $\geq$ 500000  \\ 
     \hspace*{32pt} AND Account.Balance $\leq$ 5000000000\\
{\bf GROUP BY} Branch\_ID\\
{\bf ORDER BY} Branch\_ID; 
}
\end{framed}
\caption{SQL query to ``Count the number of customers in each branch having age between 30 and 70 and account balance between 500,000 to 5,000,000,000''}
\label{fig:formQuery}
\end{figure}


As we can see, writing the above SQL query requires every analyst to be familiar with the bank's database schema and SQL syntax, which is not always the case. So what a bank usually does is to develop an application to help the analysts. These applications usually exposes a form based interface for the analysts to vary the parameters and issue SQL queries abstracting out the technical details of the underlying database. In the above case, the form exposed by the application will have four fields to select the upper and lower limit for the customer's age and the upper and lower limit for the account balance. The analyst can put in different values for these input fields and then check the output. 
Note that there is no need for the Analyst to be employee of the Bank. He could be a auditor or regulator or any third party hired by the bank to do analysis. 

If the bank has adopted the DBaaS model, the bank's database would then be outsourced to some third party cloud Service Provider. For secure query processing (focusing only on range predicates in the query) he must then use some secure encryption mechanism. The analyst can continue to use the same application interface as before but the applications will now use Security Agent as the database gateway.
The Analyst has now became a client in the DBaaS model, querying the bank's encrypted database.

In the \adv adversarial model, the Service Provider and Client can collude and can very easily launch active attacks on the system. Client (Analyst) will fill in the form with the plain text values for which he wants the ciphertext. Then he will ask the application to run the query. The application will in turn pass the query to the Security Agent. The Security Agent will transform the query, encrypting the form fields and then send this transformed query to the Service Provider. The Service Provider will see the transformed query and will be able to see the encrypted values of the constants put in the form field. Since Service Provider and Client are colluding they will immediately know the ciphertext corresponding to the plaintext value which the client had input into the form. Also, after executing the query over encrypted database Service Provider will be able to identify the tuples which satisfy the query predicates. Furthermore there is nothing stopping the Adversary from giving a second query. 
Hence the Adversary will be able to launch \emph{Adaptive Chosen Plaintext attack}~\ref{subsubsec:adaptive} as well as \emph{Query Injecting attack}~\ref{subsubsec:qi} on the system.
%Later in Section~\ref{sec:existingSolns} we will show that none of the existing solutions that allow the evaluation of range predicates over encrypted data are secure under this adversarial setting.

One important point to consider here is that the Adversary will not be able to mount a \emph{Chosen Ciphertext attack} on the system. This is because of the assumption that the Adversary is honest in executing the protocols. He is not allowed to tamper with the working of the database engine. If he had been able to tamper with the database engine then Adversary (acting as Service Provider) will insert a fake tuple with the ciphertext value which he wanted to decrypt. Then this row will be decrypted by the Security Agent and passed to the Adversary (acting as Client). Hence the net result will be that Adversary will be able to get the plaintext of his chosen ciphertext and he will be able to mount a chosen ciphertext attack in the system.

The above example shows that the \adv adversarial model is in fact an actual representation of the real world scenario. All the DBaaS solutions should be evaluated for security in this model rather than in the weaker Honest-but-Curious security model.