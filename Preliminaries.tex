\section{Preliminaries}
\label{sec:prelim}

We will first describe the notations, followed by some basic definitions and concepts that are used in the rest of the paper. 
%\begin{enumerate}

%\item {\bf Notations:}  
\subsection{Notations} 
The security of encryption schemes is specified in terms of a security parameter, denoted by $\lambda$. This takes an integer value and is chosen when the encryption scheme is initialized.
\comment{, for e.g. when the keys are generated} This value is assumed to be known to the adversary attacking the scheme. The running time of all the algorithms, including those of the adversary and the honest parties, as well as the adversary's success probability are all viewed as functions of the security parameter. \comment{The algorithms in this paper implicitly takes the security parameter as an additional input.}

If $N$ is an integer, then $[N]$ denotes the set $\{1,\cdots , N\}$. For a set $S$, $x \xleftarrow{\$} S$ denotes that $x$ is selected uniformly at random from set $S$ and $|x|$ denotes its length in bits. $x[m \cdots n]$ denotes extracting bits $m$ to $n$ from the bit representation of $x$, where the bit $m$ lies towards the most significant bit of $x$. $x_1||\cdots||x_n$ denotes an encoding of $x_1, \cdots, x_n$ from which each $x_i$ is uniquely recoverable. 
Further, let $\mathcal{P}$ denote the domain of a plaintext message. $[x]_n \xleftarrow{\$} \mathcal{P}$ denotes a set of $n$ plaintexts that are selected uniformly at random from the domain $\mathcal{P}$.

\subsection{Terminology}
The definitions in this section are taken from \cite{kl,handbook}.
\\
%\item 
\noindent{\bf Probabilistic Polynomial-Time (PPT) Algorithms: } Probabilistic algorithms that run in time \emph{polynomial in $\lambda$} are considered as \emph{probabilistic polynomial-time (PPT) algorithms}. Formally, this implies that the algorithm runs in time $a \cdot \lambda^{c}$, for some constants $a$ and $c$. 
\comment{The adversaries and the honest parties in the system are considered as algorithms.
We require that honest parties run in polynomial-time} The goal of most encryption schemes (including ours) is to achieve security against PPT adversaries, since adversarial strategies that require a super-polynomial amount of time are not considered realistic threats.% (and so are not considered for evaluation of our scheme).
\\
%\item 
\noindent{\bf Negligible Success Probability}: An encryption scheme is said to be secure, if for any \emph{PPT adversary}, there exists an integer $N$ such that for all integers $\lambda > N$ the probability that the adversary succeeds in breaking the scheme is $f(\lambda) < 1/p(\lambda)$, where $p(\cdot)$ is any polynomial in $\lambda$. It means that for every constant $c$ the adversary's success probability is smaller than $\lambda^{-c}$ for large enough values of $\lambda$. A function that grows smaller than any inverse polynomial is called \emph{negligible} and the above function $f(\lambda)$ denotes the \emph{negligible success probability} of the adversary.
\comment{
 A negligible function satisfies the following property:
\comment{{\bf Properties of Negligible Functions}:} if $f_1(\lambda)$ and $f_2(\lambda)$ are two negligible functions then
\begin{enumerate}
\item $f_3(\lambda)$ = $f_1(\lambda) + f_2(\lambda)$ is also negligible.
\item $f_4(\lambda)$ = $p(\lambda) \cdot f_1(\lambda)$ is also negligible, where $p(\lambda)$ is any positive polynomial.
\end{enumerate}
}
%\item 
\comment{
\noindent{\bf Strong Pseudorandom Permutation: } 
\label{def:prp}
Let $F:\{0,1\}^* \times \{0,1\}^* \rightarrow \{0,1\}^* $ be an efficient keyed permutation. $F$ is said to be a strong pseudo-random permutation if for all probabilistic polynomial-time distinguishers $\mathcal{D}$, there exists a negligible function $n(\lambda)$ such that:
\begin{equation}
\label{eq:prprp}
\begin{split}
|Pr[\mathcal{D}^{F_k\comment{(\cdot)},F_k^{-1}\comment{(\cdot)}}(\lambda) = 1] - Pr[\mathcal{D}^{f\comment{(\cdot)},f^{-1}\comment{(\cdot)}}(\lambda) = 1]| %\\
\leq n(\lambda),
\end{split}
\end{equation}
where $k \leftarrow \{0,1\}^{\lambda}$ is chosen uniformly at random and $f$ is chosen uniformly at random from the set of permutations on n-bit strings.
This definition can be better understood by a game between a $PRP/RP$ Challenger $\mathcal{C}$ and the Distinguisher $\mathcal{D}$ as shown in Figure~\ref{fig:prp/rp}.
\\
%\item 

\noindent{\bf Block Cipher: } A block cipher is an efficient, keyed permutation $F : \{0,1\}^n \times \{0,1\}^l \rightarrow \{0,1\}^l$. We refer to $n$ as the \emph{key length} and $l$ as the \emph{block length} of $F$. The objective of a block cipher is to provide confidentiality and the corresponding objective of an adversary is to recover plaintext from the ciphertext. A block cipher is \emph{totally broken} if a key can be found or if the plaintext is revealed from the ciphertext, and \emph{partially broken} if an adversary is able to recover part of the plaintext (but not the key) from the ciphertext.

\comment{
Block ciphers are viewed as \emph{building blocks} for encryption and other schemes, and not as encryption scheme themselves. To evaluate block cipher security, it is customary to always assume that an adversary (i) has access to the ciphertext data; and (ii) knows all the details of the encryption function except the secret key. Under these assumptions, attacks are classified based on what information a cryptanalyst has access to in addition to the ciphertext. The class of attacks considered in this paper are mentioned in Section~\ref{subsec:attackModels}.
}

The measure of security for practical block ciphers (for ex.  AES) is the complexity of the best (currently) known attack. A number of block ciphers that are used in practice have withstood many years of public scrutiny and attempted cryptanalysis, and, given this fact, it is quite reasonable to assume that these block ciphers are indeed secure. For example, as of today no better attack than exhaustive key search is known for full AES construction. \comment{ (even the complexity of the attacks on the variants of AES is very high).}

%\end{enumerate}

\begin{figure}[h!] 
\begin{framed}
\small
{\bf The PRP/RP Indistinguishability Experiment } $Exp_{\mathcal{D}}^{PRP/RP}(\lambda)$:
\begin{enumerate}
\item The Challenger $\mathcal{C}$ chooses a bit  $b \xleftarrow{\$} \{0,1\}$, if $b = 0$, then $\mathcal{C}$ uses a PRP ($F_k(\cdot)$) to answer $\mathcal{D}'s$ permutation and inverse permutation queries otherwise $\mathcal{C}$ selects a function $f$ uniformly at random from the set of permutations on n-bit strings.

\item The Distinguisher $\mathcal{D}$ makes polynomial number of permutation and inverse permutation queries to $\mathcal{C}$. \comment{to obtain the corresponding results.} At the end $\mathcal{D}$ outputs one bit $b'$, as its guess of bit $b$.
\comment{
\item The output of the experiment is the bit $b'$.
}
\end{enumerate}
\end{framed}
\caption{PRP/RP Indistinguishability Game \comment{between Challenger $\mathcal{C}$ and Distinguisher $\mathcal{D}$}}
\label{fig:prp/rp}
\end{figure}
}


\subsection{Order-Preserving Encryption}
\label{subsec:ope}

\emph{\comment{Order-Preserving encryption} OPE} \cite{openum,opebold,boldy12,opepopa} is an important class of encryption functions to support range query processing over encrypted databases. These are deterministic encryption schemes that preserve the numerical ordering of plaintext in the encrypted domain. The transformation of a range query on the plaintext to a query on the encrypted data is straightforward, due to the order preserving property. The encrypted values of the range endpoints form the endpoints of the range in the encrypted domain.  Following definition of OPE is adapted from \cite{opebold}. 

\begin{defnsub}
{\bf (\emph{Order-Preserving Encryption})}
\label{opedefn}
For $A,B \subseteq \mathbb{N}$ with $|A| \leq |B|$, a function $f: A \rightarrow B$ is order-preserving (aka. strictly-increasing) if for all $i,j \in A$, $f(i) > f(j)$ iff $i > j$. We say that deterministic encryption scheme $\mathcal{SE} = (\mathcal{K}, Enc, Dec)$ with plaintext and ciphertext-spaces $\mathcal{P}$ and $\mathcal{R}$ is order-preserving if $Enc(K,\cdot)$ is an order-preserving function from $\mathcal{P}$ to $\mathcal{R}$ for all $K$ output by $\mathcal{K}$ (with elements of $\mathcal{P}$ and $\mathcal{R}$ interpreted as numbers, encoded as strings).
\end{defnsub}


\subsection{Prefix-Preserving Encryption}
\label{subsec:ppe}
The PPE scheme was initially proposed by Xu et al.~\cite{ppe} for prefix-preserving IP address anonymization. The content in this subsection is adapted from ~\cite{ppe,ppetree}.

\begin{defnsub}
{\bf (\emph{Prefix-Preserving Encryption})}
\label{ppedefn}
We say that two n-bit numbers $a = a_1a_2...a_n$ and $b = b_1b_2...b_n$ share a k-bit prefix ($0 \leq k \leq n$), if $a_1a_2\cdots a_k = b_1b_2\cdots b_k$, and $a_{k+1} \neq b_{k+1}$ when $k < n$. An encryption function $E_p$ is defined as a one-to-one function from $\{0,1\}^n$ to $\{0,1\}^n$. An encryption function $E_p$ is said to be prefix-preserving, if, given two numbers $a$ and $b$ that share a $k-bit$ prefix, $E_p(a)$ and $E_p(b)$ also share a $k-bit$ prefix.
\end{defnsub}

\comment{

\begin{figure}[H]
\includegraphics[width=\linewidth]{ppeTrees.png}
\caption{An example of prefix-preserving encryption}
\label{fig:ppetrees}
\end{figure}

It is helpful to consider a geometric interpretation of prefix-preserving encryption~\cite{ppe}. If a plain text can take any value of a \emph{n-bit} number, the entire set of plaintexts can be represented by a complete binary tree of height $n$. This is called the \emph{plaintext tree}. Each node in the plaintext tree (excluding the root node) corresponds to a bit position, indicated by the height of the node, and a bit value, indicated by the direction of the branch from its parent node. Figure~\ref{fig:ppetrees}(a) shows a plaintext tree (using 4-bit plaintexts for simplicity). 

A prefix-preserving encryption function can be viewed as specifying a binary variable for each non-leaf node (including the root node) of the plaintext tree. This variable specifies whether the encryption function ``flips'' this bit or not. Applying the encryption function results in the rearrangement of the plaintext tree into a \emph{ciphertext tree}. Figure \ref{fig:ppetrees}(c) shows the ciphertext tree resulting from the encryption function shown in Fig. \ref{fig:ppetrees} (b). \comment{Note that an encryption function will, therefore, consist of $2^n - 1$ binary variables, where $n$ is the length of a plaintext.

A general form of prefix-preserving encryption function is presented in~\cite{ppe}. Let $f_i$ be a function from $\{0,1\}^i$ to $\{0,1\}$, for $i = 1, 2,\cdots , n-1$ and $f_0$ is a constant function. Given a plaintext $a = a_1a_2\cdots a_n$, the ciphertext $a_1{'}a_2{'}\cdots a_n{'}$ will be computed by the algorithm given in Figure~\ref{fig:ppealgo}.

\begin{figure}[h!] 
\begin{framed}
\small
\begin{enumerate}
\item Compute $a_i{'}$ as $a_i \oplus f_{i-1}(a_1a_2\cdots a_{i-1})$, where $\oplus$ stands for the exclusive-or operation, for $i = 1, 2,\cdots, n$.
\item Return $a_1{'}a_2{'}\cdots a_n{'}$.
\end{enumerate}
\end{framed}
\caption{Prefix-preserving encryption algorithm}
\label{fig:ppealgo}
\end{figure}
}
\comment{
In \cite{ppe}, the prefix-preserving encryption scheme is defined as instantiating functions $f_i$ with cryptographically strong stream ciphers or block ciphers as follows:
\begin{equation}
f_i(a_1a_2\cdots a_i) = \mathcal{L}(\mathcal{R}(a_1a_2\cdots a_i,\kappa))
\end{equation}
where $i = 1, \cdots, n - 1$ and $\mathcal{L}$ returns the ``least significant bit''. Here $\mathcal{R}$ is a pseudorandom function or a pseudorandom permutation (i.e., a block cipher). $\kappa$ is the cryptographic key used in the pseudorandom function $\mathcal{R}$. Its length should follow the guideline specified for the pseudorandom function that is actually adopted. 

The encryption function can be performed quickly as it only involves $n - 1$ symmetric key cryptographic operations, and these $n - 1$ operations can be done in parallel. }
A prefix expresses a prefix range, thus a prefix-matching query can be efficiently processed as a range query with a $B^+-tree$ index structure.} 
The transformation of range query to prefix query is based on the fact that an arbitrary interval can be converted into a union of \emph{prefix ranges}, where a prefix range is one that can be expressed by a prefix. For example, the interval $[32, 111]$, the \emph{8-bit} binary representation of which is $[00100000, 01101111]$, can be represented by a set of prefixes $\{001*, 010*, 0110*\}$, where the notation $*$ is used to denote an arbitrary suffix. The prefix ranges in the plaintext domain are converted to prefix ranges in the encrypted domain for querying by applying the same encryption function. The number of prefixes needed to represent an interval over $n$ bit data is bounded by $2(n - 1)$.
\comment{
\comment{To verify that a number is in the interval is equivalent to check that the number matches any of those prefixes in the set. For example, $37$ ($00100101$ in binary) is in the interval as it matches prefix $001*$, while $128$ ($10000000$ in binary) is not in the interval since it matches none of those three prefixes. }

\comment{

\begin{figure}[h!] 
\begin{framed}
\begin{enumerate}
\small

\item Starting from $k = 1$, find the most significant bit, numbered $k$, for which $a_k < b_k$.

\item If $k$ is not found, i.e., for all $i \leq i \leq n$, $a_i = b_i$, then the interval can be denoted by prefix $a_1a_2\cdots a_n$. Return $a_1a_2\cdots a_n$.

\item If for all $k \leq i \leq n$, $a_i = 0$ and $b_i = 1$, then return $a_1a_2a_{k-1}*$ (return $*$ if $k = 1$).

\item Transform interval [$a_1a_2\cdots a_n,b_1b_2\cdots b_n$] into [$a_1\cdots a_{k-1}0a_{k+1}\cdots a_n,a_1\cdots a_{k-1}011\cdots 1$] $\cup$ [$a_1\cdots a_{k-1}100\cdots 0,a_1\cdots a_{k-1}1b_{k+1}\cdots b_n$].

\item Run this algorithm with interval [$a_{k+1}\cdots a_n,11\cdots 1$] as input, concatenate $a_1\cdots a_{k-1}0$ before all the returned prefixes. Then run this algorithm with interval [$00\cdots 0,b_{k+1}\cdots b_n$] as input, concatenate $a_1\cdots a_{k-1}1$ before all the returned prefixes. Return all the prefixes.
\end{enumerate}
\end{framed}
\caption{The algorithm for transforming interval $[a_1a_2\cdots a_n, b_1b_2\cdots b_n]$ into prefixes}
\label{fig:ppetransform}
\end{figure}

Figure~\ref{fig:ppetransform} presents a recursive algorithm to generate the set of prefixes for a given interval [$a_1a_2\cdots a_n,b_1b_2\cdots b_n$]. We have seen that matching an interval based on a set of prefix-matchings is both simple and efficient. Therefore prefix-preserving encryption algorithm can be used to efficiently support interval-matching as a query condition while preserving the confidentiality of data and queries.
}

Let $n$ denote the length of the binary representation of the data, and let
$p_n$ denote the number of prefixes needed to represent an interval. The authors of \cite{ppetree} have proved following theorem on the upper bound of $p_n$.
\begin{thm}
\label{thm:ppeupperbound}
\emph{For any interval} [$a_1a_2\cdots a_n,b_1b_2\cdots b_n$] $(n \geq 2)$, $p_n \leq 2(n - 1)$.
\end{thm}
}