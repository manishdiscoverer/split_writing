\section{The SDB Protocol}
\label{sec:sdb}
{\bf This section provides a brief insight into the encryption scheme developed in SDB approach and the protocol of comparison operator. The content is taken from \cite{sdb}.}

\subsection{Secret Sharing Protocol}

The DO maintains two secret numbers, $g$ and $n$. The number $n$ is generated according to the RSA method, i.e. $n$ is the product of two big random prime numbers $\rho_1$ and $\rho_2$. The number $g$ is a positive number that is co-prime with $n$, in the implementation $\rho_1$ and $\rho_2$ are 512-bit numbers and $n$ is 1024-bit. Define,
\begin{align}
 \phi(n) = (\rho_1 - 1)(\rho_2 - 1)
\end{align}
Based on the property of RSA,\\
($a^{ed}$ mod $n$ = $a$) $\forall$ $a,e,d$ such that $ed$ mod $\phi$($n$) = 1.

Consider a sensitive column $A$ of a relational table $T$ of $N$ rows $t_1,...,t_N$. The DO assigns to each row $t_i$ in $T$ a distinct random row-id $r_i$. Moreover, the DO randomly generates a column key $ck_A = ~<m,x>$, which consists of a pair of random numbers, such that $0 <r_i,m,x < n$. 

To simplify the discussion, let us assume that the schema of $T$ is (row-id, $A$). The idea is to store table $T$ encrypted on the SP. This consists of two parts: 
\begin{enumerate}
\item Sensitive values in column $A$ are encrypted using secret sharing based on the column key $ck_A = ~<m,x>$ and the row-ids.
\item Since the row-ids are used in encrypting column $A$'s values, the row-ids have to be encrypted themselves.
\end{enumerate}  
The row-id is encrypted using an additive homomorphic encryption e.g. SIES ${\cite{sies}}$, on the other hand, sensitive data is encrypted using secret sharing scheme. The secret sharing encryption process consists of two steps:\\
{\bf Step 1 (Item key generation)}: Given a row id $r$ and a column key $ck_A =~<m,x>$, the item key $v_k$ is given by,
\begin{equation}
v_k = mg^{(rx ~mod ~\phi(n))}~ mod ~n.
\end{equation}
{\bf Step 2 (Share Computation)}: Given a sensitive value $[[v]]$ and its item key $v_k$, the encrypted value $v_e$ is given by,
\begin{equation}
v_e = \mathcal{E}([[v]], v_k) = [[v]]v_k^{-1} ~ mod ~n
\end{equation}
where $v_k^{-1}$ denotes the modular multiplicative inverse of $v_k$, i.e., $v_kv_k^{-1}$ mod $n$ = 1.\\\\
To recover $[[v]]$, one needs both shares $v_k$ and $v_e$ and compute
\begin{equation}
[[v]] = \mathcal{D}(v_e, v_k) = v_ev_k~mod~n.
\end{equation}

\begin{figure}[h!] 
  \centering
    \includegraphics[width=0.4\textwidth]{fig1}
     \caption{Encryption procedure (g = 2, n = 35)}
\end{figure}

Figure 1 summarizes the whole encryption procedure and illustrates how sensitive data (e.g., a column $A$) is transformed into encrypted values $v_e$'s. It also shows that DO only needs to maintain a column key in its key store, while the SP stores the bulk of the data.

An attacker may obtain three kinds of knowledge by hacking the SP:\begin{itemize}
\item \emph{Database (DB) Knowledge -} The attacker sees the encrypted values $\emph{v}_e$'s stored in the DBMS of the SP.
\item \emph{Chosen Plaintext Attack (CPA) Knowledge -} The attacker is able to select a set of plaintext values [[\emph{v}]]'s and observe their associated encrypted values $\emph{v}_e$'s.
\item \emph{Query Result (QR) Knowledge -} The attacker is able to see the queries submitted to the SP and all the intermediate (encrypted) results of the operator involved in the query.
\end{itemize}

The security goal in SDB is to prevent the attacker from recovering plaintext values [[\emph{v}]]'s given that the attacker has acquired certain combinations of knowledge listed above. {\bf SDB is designed to be secure against the following threats}:
\begin{itemize}
\item \emph{\bf DB + CPA Threat :} The attacker has both DB knowledge and CPA knowledge.
\item \emph{\bf DB + QR Threat :} The attacker has both DB knowledge and QR knowledge.
\end{itemize}

SDB supports several database operators. Each operator is executed by a protocol, which consists of a client protocol and a server protocol. The client protocol takes column key(s) as input and produces column key(s) as output, while the sever protocol's input and output are column(s) of encrypted values.

A column operand of an operator is not necessarily sensitive, thus SDB considers three modes for the operators, namely,
\begin{itemize}
\item {\bf EE mode}: Both operands are encrypted.
\item {\bf EP mode}: One operand is encrypted and the other is  plaintext.
\item {\bf EC mode}: One operand is encrypted and the other is a constant.
\end{itemize}

To properly implement the various secure operators, SDB add two columns: $S$ and $R$ to each table $T$. Each value in $S$ is a constant 1. Each value in $R$ is a positive random number. The columns $S$ and $R$ are encrypted using secret sharing.

Consider a sensitive column $A$ encrypted with column key $ck_A$ = $<m,x>$, the SDB protocol follows two simple properties:

{\bf PROPERTY 1:} If $ck_A$ = $<m,0>$, then all the values in column $A$ has the same item key $m$.

{\bf PROPERTY 2:} If $ck_A$ = $<1,0>$, then all encrypted values $v_e$'s of column $A$ are equal to their corresponding plaintext values $[[v]]$.\\
Some operators in their protocol purposely convert a column key to $<m,0>$ or even $<1,0>$. The later is typically used to reveal the plaintext of an operator's result, such as the result of a comparison operator.

Next we briefly describe the protocol for comparison operator. The proof of correctness and the proof of security of the operator is given in \cite{sdb}.

\subsection{Comparison (= / $>$)}

SDB considers two comparison operators, namely, equality (=) and ordering ($>$). Given two columns $A$ and $B$, the objective is to compute a \emph{plaintext} column $C$ such that, for each row, $c = 1$  if the comparison is ``true" and $c = 0$ otherwise.

To compute $C$, the comparison protocol first computes $Z$ = $R$ $\times$ ($A - B$), where $R$ is the random column of the table. Next, the protocol performs a key update to obtain $Z^{'} $ = $\kappa$($Z$,$<1,0>$). Hence $Z^{'}$ is equal to $Z$ but with the column key $ck_{Z^{'}}$ = $<1,0>$. By Property 2, the plaintext of $Z^{'}$ are all revealed to the server. {\bf Hence, the server observes the plaintext values of [$\bf R \times (A - B)$]}. The server protocol can deduce, for each row, (1) if $z^{'}$ = 0, then $[[a]]$ = $[[b]]$, and (2) if $z^{'}$ is positive then $[[a]] > [[b]]$.


 
