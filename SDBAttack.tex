\section{Insecurity of SDB Approach: Attack on Comparison Operator}
\label{sec:sdbAttack}

\hspace*{-10pt}{\bf {Claim 1:} SDB approach is not secure under DB + QR Security Model, if the adversary has the knowledge of only one tuple of known plaintext and ciphertext.}\\
{\bf {Proof:}} Let us consider that the cloud server acts as QR adversary (say ${\cal{C}}$) against the SDB approach. Now ${\cal{C}}$ will follow below steps to compute the plaintext values of encrypted column. 
\begin{enumerate}
\item ${\cal{C}}$ will first observe a few number ($>=$ 3) of comparison operations, on some encrypted column say $A$, that is involved in a correlated sub query doing comparison with unknown constants. We will use these comparisons to get the scaled value of random valued column $R$. This we will call as {\emph{Result1}}.
\item Next ${\cal{C}}$ will use the instructions sent from client to server to answer $A>B$ comparison query, to curiously compute $A>-B$ comparison, where $A$ is the encrypted column involved in the comparisons done in step 1 and $B$ is any other column. This we will call as {\emph{Result2}}.
\item Now after computing {\emph{Result1}} and {\emph{Result2}} from above steps, ${\cal{C}}$ will mathematically combine them to expose some scaled value of column $A$.
\item Now, given only one pair of plaintext and ciphertext (i.e. plaintext as well as encrypted value of only one tuple of $A$), ${\cal{C}}$ will be able to compute the exact values of all the tuples in column $A$.
\end{enumerate}

Thus we see that by following above steps ${\cal{C}}$ is able to compute and reveal the plaintext values of all the tuples of encrypted column $A$, hence SDB cannot be accepted as a general purpose solution in real applications.\\
Let us see the above steps in greater details:\\\\
{ \bf Step 1: Computing the scaled value of random valued column $R$.}

Suppose over a period of time following comparison queries have been made by the client to the cloud server as part of a correlated query,
\begin{itemize}
\item $A > k_1$
\item $A > k_2$
\item $A > k_3$
\end{itemize}
In general, SP cannot distinguish whether an operation is done on $A$ or on some constant multiple of $A$, so let us consider the general form of above equations:
\begin{itemize}
\item $\alpha A > k_1$
\item $\alpha A > k_2$
\item $\alpha A > k_3$
\end{itemize}

In the above comparisons $A$ and $B$ are encrypted database columns while $k_1, ~k_2, ~k_3 ~and ~\alpha$ are unknown constants. 

When above comparisons are performed, then according to the comparison protocol, following equations can be inferred by the server with their plaintext values,
\begin{align}
[R \times (\alpha A - k_1 \times S)] = C_1~(say) \\
[R \times (\alpha A - k_2 \times S)] = C_2~(say) \\
[R \times (\alpha A - k_3 \times S)] = C_3~(say) 
\end{align}
%[R \times (\alpha A - \beta B)] = C_4~(say) 
In the above equations, all the values on the right hand side are available in plaintext at server side, while the left hand side in {[}~{]} is inferred by the server because of its knowledge of the comparison protocol. $R$ denotes the random value column and $S$ denotes column with constant value 1, these are the two additional encrypted columns added by DO, before sending the encrypted database to the cloud server SP.\\
Although all the columns $R$, $A$ and $S$ as well as $k_i$'s involved in the left hand side of above equations within {[~]} are encrypted and their actual values are not known to the server, but the server knows that the plaintext values on the right hand side of above equations are computed by evaluating the expressions on the plaintext values of respective columns and constants involved in that equation. Therefore we can rewrite above equations as:
\begin{align}
[R \times (\alpha A - K_1)] = C_1 \\
[R \times (\alpha A - K_2)] = C_2 \\
[R \times (\alpha A - K_3)] = C_3 
\end{align}
where, $K_1$ is a vector with constant value $k_1$, $K_2$ is a vector with constant value $k_2$ and $K_3$ is a vector with constant value $k_3$. \\
Now, subtracting equation (23) from equation (24), we get,
\begin{align*}
[R \times K_1 - R \times K_2] = C_2 - C_1 \\
\Rightarrow [R \times (K_1 - K_2)] = C_2 - C_1
\end{align*}
Let $Y_1$ = $(K_1 - K_2)$ and $Z_1$ = $(C_2 - C_1)$, therefore we can write above equation as,
\begin{align}
[R \times Y_1] = Z_1
\end{align} 
Suppose there are $N$ tuples in the database, then we can write equation (26) as the element-wise product of two vectors:
\begin{align*}
\left(
\begin{array}{c}
r_1 \\
r_2 \\
r_3 \\
. \\
. \\
. \\
r_N
\end{array}
\right)
\times
\left(
\begin{array}{c}
y_1 \\
y_1 \\
y_1 \\
. \\
. \\
. \\
y_1
\end{array}
\right)
=
\left(
\begin{array}{c}
z_{11} \\
z_{12} \\
z_{13} \\
. \\
. \\
. \\
z_{1N}
\end{array}
\right)
\end{align*}
\begin{align}
\Rightarrow
\left(
\begin{array}{c}
r_1 \times y_1\\
r_2 \times y_1\\
r_3 \times y_1\\
. \\
. \\
. \\
r_N \times y_1
\end{array}
\right)
=
\left(
\begin{array}{c}
z_{11} \\
z_{12} \\
z_{13} \\
. \\
. \\
. \\
z_{1N}
\end{array}
\right)
\end{align}
Lets rewrite above equation as:
\begin{equation}
\begin{split}
\left(
\begin{array}{ccccccc}
r_1 \times y_1 & r_2 \times y_1 & r_3 \times y_1 & . & . & . & r_N \times y_1
\end{array}
\right) \\
=
\left(
\begin{array}{ccccccc}
z_{11} & z_{12} & z_{13} & . & . & . & z_{1N}
\end{array}
\right)
\end{split}
\end{equation}
Similarly, by subtracting equation (23) from equation (25) we get,
\begin{align*}
[R \times K_1 - R \times K_3] = C_3 - C_1 \\
\Rightarrow [R \times (K_1 - K_3)] = C_3 - C_1
\end{align*}
Let $Y_2$ = $(K_1 - K_3)$ and $Z_2$ = $(C_3 - C_1)$, therefore we can write above equation as,
\begin{align}
[R \times Y_2] = Z_2
\end{align} 
Following the previous steps, we will get:
\begin{equation}
\begin{split}
\left(
\begin{array}{ccccccc}
r_1 \times y_2 & r_2 \times y_2 & r_3 \times y_2 & . & . & . & r_N \times y_2
\end{array}
\right) \\
=
\left(
\begin{array}{ccccccc}
z_{21} & z_{22} & z_{23} & . & . & . & z_{2N}
\end{array}
\right)
\end{split}
\end{equation}
Now let's combine equation (28) and (30) in one matrix, we get
\begin{equation}
\begin{split}
\left(
\begin{array}{ccccccc}
r_1 \times y_1 & r_2 \times y_1 & r_3 \times y_1 & . & . & . & r_N \times y_1 \\
r_1 \times y_2 & r_2 \times y_2 & r_3 \times y_2 & . & . & . & r_N \times y_2 
\end{array}
\right) \\
=
\left(
\begin{array}{ccccccc}
z_{11} & z_{12} & z_{13} & . & . & . & z_{1N} \\
z_{21} & z_{22} & z_{23} & . & . & . & z_{2N}
\end{array}
\right)
\end{split}
\end{equation}
Now taking the column wise GCD of the above matrix we get,
\begin{align*}
GCD([r_1 \times y_1], [r_1 \times y_2]) = GCD(z_{11}, z_{21})\hspace{40pt} \\
= [r_1]~ if~ [y_1] ~and ~[y_2] ~are ~relatively~ prime \\
= [r_1] \times GCD([y_1], [y_2]) ~otherwise \hspace{30pt}
\end{align*}
Let us take the case when [$y_1$] and [$y_2$] are not relatively prime and let,
\begin{align*}
GCD([y_1], [y_2]) = [c] 
\end{align*}
where [c] is some constant.
\begin{align*}
\Rightarrow GCD([r_1 \times y_1], [r_1 \times y_2]) = [r_1] \times [c] \\
= [r_1 \cdot c]
\end{align*}
Similarly, we can compute the rest of $[r_i \cdot c]$, where $2 \leq i \leq  n$. {\bf Thus now we have $[R \cdot c]$ exposed in plaintext at the server. This we will call as  {\emph{Result1}}}.\\\\
{ \bf Step 2: Computing $A>-B$ comparison from the instructions of $A>B$ comparison query.}

To compute $A>B$ following steps are performed:
\begin{enumerate}
\item Client chooses a new column key $<$$m_C, x_C$$>$.
\item Now client issues the key update operations for columns $A$ and $B$, to the new column key $<$$m_C, x_C$$>$.
\item Thus server now has:
\begin{itemize}
\item $\kappa$(A, $<$$m_C, x_C$$>$) = $A_e^{'}$
\item $\kappa$(B, $<$$m_C, x_C$$>$) = $B_e^{'}$
\end{itemize}
\item Next client asks server to compute $C_e = A_e^{'} - B_e^{'}$.
\item Now following the comparison protocol, server performs EE multiplication between encrypted column $R$ and newly constructed column $C$, thus server stores $D_e = R_e \times C_e$ and client sets column key for $D$ as $ck_D = ~<m_D, x_D>$ = $<m_R \cdot m_C, x_R + x_C>$.
\item Now server performs a key update on column $D$ using column key $<1,0>$.
\end{enumerate}
By performing above steps client exposes $[R \times (A - B)]$ in plaintext to the server.\\
Now server can compute the result of $A > -B$ comparison by only modifying the step 4 of above comparison protocol, the modification is as follows:
\begin{itemize}
\item Instead of computing $C_e = A_e^{'} - B_e^{'}$, server computes $C_e = A_e^{'} + B_e^{'}$.
\end{itemize} 
Rest all the steps 1, 2, 3, 5 and 6 are followed as described previously.\\
We can easily verify that following above steps, the \emph{QR} adversary can compute the plaintext values of $[R \times (A + B)]$. From the adversarial point of view, SP can only infer the plaintext values of following equations :
\begin{align}
[R \times (\delta A - \beta B)] = C_5 ~(say), and\\
[R \times (\delta A + \beta B)] = C_6 ~(say)\hspace*{20pt}
\end{align}
where, $\delta$ and $\beta$ are unknown constants and $C_5$ and $C_6$ are exposed in plaintext to the server. We will refer above equations as \emph{Result2}.\\\\
{\bf Step 3: Exposing scaled value of column $A$.}\\
Adding equations (32) and (33), we get,
\begin{align}
[2 \times R \times \delta A] = C_5 + C_6
\end{align}
Let $Z_3$ = $(C_5 + C_6)$ / 2, 
\begin{align}
\Rightarrow [R \times \delta A] = Z_3
\end{align}
Dividing equation (35) by \emph{Result1}, i.e. $[R \cdot c]$, we get,
\begin{align}
\left[ \frac{\delta A}{c} \right] = \frac{Z_3}{[R \cdot c]}
\end{align}
Let $\gamma = \frac{\delta}{c}$, then
\begin{align}
\left[ \gamma A \right] = \frac{Z_3}{[R \cdot c]}
\end{align}
Thus the above equation exposes the plaintext values of column $A$ multiplied by some unknown constant $\gamma$ at the server side. Similarly, following the same approach we can expose the scaled values of other columns also.\\\\
{\bf Step 4: Exposing exact values of columns.}\\
Now if SP knows only one pair of plaintext and ciphertext value of any tuple in column $A$ , then it can compute the value of $\gamma$, let $a_i$ be the plaintext value of some tuple in column $A$, then $\gamma$ will be computed as below,
\begin{align}
\frac{\gamma \cdot a_i}{(a_i)} = \gamma
\end{align}
where $\gamma \cdot a_i$ is the corresponding tuple of column $A$ exposed as the result of Step 3.

{\bf Once the server is able to compute the value of $\gamma$, then it can expose all the tuples in column $A$. }

